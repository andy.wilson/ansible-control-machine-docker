# Dockerized Ansible Control Machine

[![pipeline status](https://gitlab.com/andy.wilson/ansible-control-machine-docker/badges/master/pipeline.svg)](https://gitlab.com/andy.wilson/ansible-control-machine-docker/commits/master)
[![PyPI](https://img.shields.io/pypi/v/ansible.svg)](https://pypi.org/project/ansible/)
[![PyPI - Status](https://img.shields.io/pypi/status/ansible.svg)](https://pypi.org/project/ansible/)

> Running Ansible inside a container

![ansible-docker-logo](ansible-docker.png)

## **NEW!**

Now featuring a daily build with container vulnerability scanning

## Installing & Getting started

Ensure you have `docker` and `docker-compose` installed.

You can download them from <https://www.docker.com/get-docker> or with the package manager for your OS

Now just:

```shell
git clone git@gitlab.com:andy.wilson/ansible-control-machine-docker.git
cd ansible-control-machine-docker
```

Edit the `docker-compose.yaml` file and replace the volumes with correct locations for your filesystem.

## Running

Now run `docker-compose run --rm ansible-box` to bring up the box.

You will now be at a terminal looking like

```text
/repo # █
```

### Check the Ansible version

Run `ansible --version` to check the Ansible version

## Run your commands

Run any `ansible-*` commands as needed