FROM python:3.7.9-alpine3.12
LABEL maintainer="andy.wilson@integrationqa.com"
# hadolint ignore=DL3013,DL3018,DL3042
RUN set -ex; \
    apk --no-cache add \
      curl \
      gcc \
      libffi-dev \
      make \
      musl-dev \
      openssh \
      openssl-dev \
    && pip install --upgrade pip && \
    pip install \
      ansible \
      boto \
      boto3
RUN mkdir -p /repo
VOLUME [ "/repo" ]
VOLUME [ "/root/.ssh" ]
WORKDIR /repo
